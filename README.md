# Telegram Cringe Bank Bot


If someone is posting a lot of cringe you can add this bot to the chat and reply to cringy messages with /cringe.

Everyone has a cringe "bank account" which allows them to post a certain amount of cringe.
The account fills up daily.
Getting /cringe responses to media you post causes the account of the person posting cringe to be depleted.

If someone's account is empty and they still receive /cringe then the offending posts will be removed (this is why the bot requires admin privileges).

## Configuration and Setup

 - Make sure you have a running database. Refer to the [sqlalchemy documentation](https://www.sqlalchemy.org/features.html) for supported databases. The implementation is tested on SQLite and MariaDB
 - Rename the `config.yaml.example` to `config.yaml`
 - Adapt your Telegram Bot API token and database host URL. Refer to the sqlalchemy documentation for your backend for details
 - Install dependencies with `pip install -r requirements.txt`
 - You may need to install further packages for your specific database backend. For mariadb you will need `pymysql`
 - Adapt other fields to your liking
