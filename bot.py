import logging
import re
import threading
from time import sleep
from datetime import datetime, timedelta

import schedule
import yaml
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    BigInteger,
    String,
    ForeignKeyConstraint,
    create_engine,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship, sessionmaker, scoped_session
from telegram.error import BadRequest
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater


Base = declarative_base()


class Account(Base):
    __tablename__ = "accounts"
    gid = Column(BigInteger, primary_key=True, nullable=False)
    uid = Column(Integer, primary_key=True, nullable=False)
    balance = Column(Integer, nullable=False)

    cringed_to = relationship(
        "Message", secondary="cringers", back_populates="cringers"
    )


class Message(Base):
    __tablename__ = "messages"
    gid = Column(BigInteger, primary_key=True, nullable=False)
    uid = Column(Integer, primary_key=False, nullable=False)
    message_id = Column(Integer, primary_key=True)

    __table_args__ = (
        ForeignKeyConstraint(["gid", "uid"], ["accounts.gid", "accounts.uid"]),
    )

    cringers = relationship(
        "Account", secondary="cringers", back_populates="cringed_to"
    )

    time = Column(DateTime, nullable=False)


class Cringers(Base):
    __tablename__ = "cringers"
    gid = Column(BigInteger, primary_key=True, nullable=False)
    uid = Column(Integer, primary_key=True, nullable=False)
    message_id = Column(Integer, primary_key=True)

    __table_args__ = (
        ForeignKeyConstraint(["gid", "uid"], ["accounts.gid", "accounts.uid"]),
        ForeignKeyConstraint(
            ["gid", "message_id"], ["messages.gid", "messages.message_id"]
        ),
    )


def constrain(value, minimum, maximum):
    if value < minimum:
        return minimum
    if value > maximum:
        return maximum
    return value


def start(update, context):
    if update.effective_chat.id < 0:
        # Group
        text = "Hi, I'm the Cringe Bot. Just reply to a message with /cringe to mark it as cringy"
    else:
        # Single User
        text = "Hi, I'm the Cringe Bot. You should probably add me to a group."
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)


def get_account(session, uid, gid, create=True):
    current_account = session.query(Account).filter_by(gid=gid, uid=uid).first()
    if create and current_account is None:
        current_account = Account(gid=gid, uid=uid, balance=DEFAULT_BALANCE)
        session.add(current_account)
        session.commit()
    return current_account


def get_message(session, uid, gid, message_id, create=True, time_sent=datetime.now()):
    current_message = (
        session.query(Message)
        .filter_by(gid=gid, uid=uid, message_id=message_id)
        .first()
    )
    if create and current_message is None:
        current_message = Message(
            gid=gid, uid=uid, message_id=message_id, time=time_sent
        )
        session.add(current_message)
        session.commit()
    return current_message


def add_cringe_credits(uid, gid, amount):

    with DBSession() as session:
        account = get_account(session, uid, gid)

        account.balance = constrain(account.balance + amount, 0, CRINGE_MAX)

        session.commit()


def remove_cringe_credits(uid, gid, amount):
    add_cringe_credits(uid, gid, -amount)


def get_balance(uid, gid, take_lock=True):
    if not take_lock:
        with DBSession() as session:
            current_account = get_account(session, uid, gid, create=False)
            return current_account.balance

    with DBSession() as session:
        current_account = get_account(session, uid, gid)
        return current_account.balance


def add_daily_credits():
    with DBSession() as session:
        for account in session.query(Account).all():
            account.balance = constrain(account.balance + DAILY_GAIN, 0, CRINGE_MAX)
        session.commit()


def balance_single(update, context):
    uid = update.effective_message.from_user.id
    gid = update.effective_chat.id
    message_id = update.effective_message.message_id

    text = f"Your current balance is: {get_balance(uid, gid)} credits"
    context.bot.send_message(chat_id=gid, reply_to_message_id=message_id, text=text)


def balance_group(update, context):
    gid = update.effective_chat.id
    message_id = update.effective_message.message_id

    with DBSession() as session:
        accounts = session.query(Account).filter_by(gid=gid).all()

        if len(accounts) == 0:
            text = "Currently there are no balances set up for anyone in this group. They will be set up automatically when required."
            context.bot.send_message(
                chat_id=gid, reply_to_message_id=message_id, text=text
            )
            return

        text = ""
        for account in accounts:
            # First name
            try:
                text += context.bot.get_chat_member(
                    account.gid, account.uid
                ).user.first_name
                text += f": {get_balance(account.uid, account.gid, False)}\n"
            except BadRequest:
                # We don't delete old users because otherwise you could simply rejoin a group to reset your balance
                continue


        context.bot.send_message(chat_id=gid, reply_to_message_id=message_id, text=text)


def cringe(update, context):
    cringe_message = update.message.reply_to_message

    if not cringe_message:
        text = "You need to reply to a media message when using /cringe."
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            reply_to_message_id=update.message.message_id,
            text=text,
        )
        return

    # Don't allow cringing on bots
    if cringe_message.from_user.is_bot:
        text = "Cringing on bots doesn't make a whole lot of sense."
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            reply_to_message_id=update.message.message_id,
            text=text,
        )
        return

    # Don't allow cringing on posts without media
    if cringe_message.text and not re.match(URLREGEX, cringe_message.text):
        text = "Pure text messages can't be cringed on"
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            reply_to_message_id=update.message.message_id,
            text=text,
        )
        return

    # Don't allow cringing on old posts
    time_sent = cringe_message.date
    if time_sent.timestamp() < (datetime.now() - timedelta(DAYS_TO_KEEP)).timestamp():
        text = "Don't go around digging up dead bodies"
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            reply_to_message_id=update.message.message_id,
            text=text,
        )
        return

    cringe_message_id = cringe_message.message_id
    cringe_poster_uid = cringe_message.from_user.id
    gid = update.effective_chat.id
    uid = update.effective_message.from_user.id

    # Don't allow cringing on your own posts
    if uid == cringe_poster_uid:
        text = "Why would you cringe on your own message?"
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            reply_to_message_id=update.message.message_id,
            text=text,
        )
        return

    with DBSession() as session:
        # This simply ensures that the cringer exists for db integrity reasons
        get_account(session, cringe_poster_uid, gid)

        account = get_account(session, uid, gid)
        message = get_message(
            session, cringe_poster_uid, gid, cringe_message_id, time_sent=time_sent
        )
        if account in message.cringers:
            text = "You have already cringed on this message."
            context.bot.send_message(
                chat_id=gid,
                reply_to_message_id=update.message.message_id,
                text=text,
            )
            return
        else:
            message.cringers.append(account)
        session.commit()

    previous_balance = get_balance(cringe_poster_uid, gid)
    remove_cringe_credits(cringe_poster_uid, gid, CRINGE_COST)
    current_balance = get_balance(cringe_poster_uid, gid)

    if current_balance == 0:
        text = "Kinda cringe, ngl"
        context.bot.send_message(
            chat_id=gid, reply_to_message_id=cringe_message_id, text=text
        )
        context.bot.delete_message(chat_id=gid, message_id=cringe_message_id)

    # precisely calculated, non-arbitrary checkpoints
    elif current_balance <= 17 and previous_balance > 17:
        text = "Watch it kid"
        context.bot.send_message(
            chat_id=gid, reply_to_message_id=cringe_message_id, text=text
        )

    elif current_balance <= 65 and previous_balance > 65:
        text = "Consider dialing down the cringe"
        context.bot.send_message(
            chat_id=gid, reply_to_message_id=cringe_message_id, text=text
        )


def help_message(update, context):
    text = """
*You can use the following commands:*

_Cringe on a message \(needs to be a reply\)_
/cringe

_Get your individual balance_
/balance

_Get the whole groups balance_
/balance\_group
    """
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=text, parse_mode="MarkdownV2"
    )


def remove_old_messages():
    remove_before_date = datetime.now() - timedelta(DAYS_TO_KEEP)
    with DBSession() as session:
        session.query(Cringers)\
                .filter(Message.message_id == Cringers.message_id,
                        Message.gid == Cringers.gid,
                        Message.time < remove_before_date).delete(synchronize_session=False)
        session.query(Message).filter(Message.time < remove_before_date).delete()
        session.commit()


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )

    # Taken from stackoverflow, I'm just gonna trust it
    URLREGEX = re.compile(
        r"(\w+://)?"  # protocol                      (optional)
        r"(\w+\.)?"  # host                          (optional)
        r"((\w+)\.(\w+))"  # domain
        r"(\.\w+)*"  # top-level domain              (optional, can have > 1)
        r"([\w\-\._\~/]*)*(?<!\.)"  # path, params, anchors, etc.   (optional)
    )

    ACCOUNTS_LOCK = threading.Lock()
    CRINGES_LOCK = threading.Lock()

    DEFAULT_BALANCE = 0
    # TODO configuration or dynamic
    CRINGE_COST = 5
    DAYS_TO_KEEP = 1
    with open("config.yaml") as config_file:
        config = yaml.safe_load(config_file)
        updater = Updater(token=config["token"], use_context=True)
        DEFAULT_BALANCE = config["default_start_balance"]
        DAILY_GAIN = config["default_daily_gain"]
        CRINGE_MAX = config["default_cringe_max"]
        CRINGE_COST = config["default_cringe_cost"]
        db_engine = create_engine(config["db_host"], pool_pre_ping=True)
        session_factory = sessionmaker(bind=db_engine)
        DBSession = scoped_session(session_factory)
        Base.metadata.create_all(db_engine)

    updater.dispatcher.add_handler(CommandHandler("start", start))
    updater.dispatcher.add_handler(
        CommandHandler("cringe", cringe, filters=~Filters.update.edited_message)
    )
    updater.dispatcher.add_handler(CommandHandler("balance", balance_single))
    updater.dispatcher.add_handler(CommandHandler("balance_group", balance_group))
    updater.dispatcher.add_handler(CommandHandler("help", help_message))

    schedule.every().day.do(add_daily_credits)
    schedule.every(6).hours.do(remove_old_messages)
    updater.start_polling()

    while True:
        schedule.run_pending()
        sleep(10)
